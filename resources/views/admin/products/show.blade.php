@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')

    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">List Products</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Products</li>
                </ol>
                <div class="container-fluid px-4">
                    <a href="#" data-bs-toggle="modal" data-bs-target="#CreateProductModal" class="btn btn-success">Create
                        Product</a>
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <form method="GET" class="m-md-3 d-flex">
                                <input type="search" name="key" value="{{ request('key') }}" class="form-control"
                                       placeholder="search ...">
                                <button type="submit" class="btn btn-primary">
                                    search
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Category</th>
                                <th scope="col">Price</th>
                                <th scope="col">Description</th>
                                <th scope="col">Image</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">{{$product->id}}</th>
                                <td>{{$product->name}}</td>
                                @foreach($product->categories as $category)
                                    <td>{{$category->name}}</td>
                                @endforeach
                                <td>{{$product->price}}</td>
                                <td>{{$product->description}}</td>
                                <td>
                                    <img src="{{ asset('uploads/products/'.$product->image) }}" width="80px" alt="">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="container-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

