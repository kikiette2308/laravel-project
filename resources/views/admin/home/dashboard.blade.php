@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Dashboard</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Xin chào {{ auth()->user()->name }}
                        @hasRole('super_admin')
                        - Bạn đang là Supper Admin
                        @endhasRole

                        @hasRole('admin')
                        - Bạn đang là Admin
                        @endhasRole

                        @hasRole('user')
                        - Bạn đang là User
                        @endhasRole
                    </li>

                </ol>
                <div class="row justify-content-center">
                    @hasPermission('user_view')
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-dark text-white mb-4">
                            <div class="card-body">
                                <i class="fas fa-users"></i>
                                Users Management
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="counter" style="font-size: 30px">{{ $users }}</span>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('users.index') }}">View
                                    Users</a>

                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    @endhasPermission
                    @hasPermission('role_view')
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-dark text-white mb-4">
                            <div class="card-body">
                                <i class="fas fa-cogs"></i>
                                Roles Management
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="counter-item" style="font-size: 30px">{{ $roles }}</span>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('roles.index') }}">View
                                    Roles</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    @endhasPermission
                    @hasPermission('category_view')
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-dark text-white mb-4">
                            <div class="card-body">
                                <i class="fas fa-calendar-alt"></i>
                                Categories Management
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="counter" style="font-size: 30px">{{ $categories }}</span>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('categories.index') }}">View
                                    Categories</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    @endhasPermission
                    @hasPermission('product_view')
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-dark text-white mb-4">
                            <div class="card-body">
                                <i class="fas fa-shopping-cart"></i>
                                Products Management
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                <span class="counter" style="font-size: 30px">{{ $products }}</span>
                            </div>
                            <div class="card-footer d-flex align-items-center justify-content-between">
                                <a class="small text-white stretched-link" href="{{ route('products.index') }}">View
                                    Products</a>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </div>
                    </div>
                    @endhasPermission
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
