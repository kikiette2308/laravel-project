@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create Role</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Role</li>
                </ol>

                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{ route('roles.store') }}" method="POST">
                            @csrf

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control" placeholder="Name...">
                                    @error('name')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Display_Name</label>
                                    <input name="display_name" type="text" class="form-control"
                                           placeholder="Display Name...">
                                    @error('display_name')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <input class="form-check-input select-all-permission" type="checkbox">
                                <label class="form-label">Permission</label>
                                <div class="row">
                                    <div class="form-check-inline">
                                        @foreach($permissionGroup as $group => $permission)
                                            <div class="col-md-3">
                                                <input class="form-check-input select-item" type="checkbox">
                                                <label class="form-label"><b>{{ $group }}</b></label>
                                                @foreach($permission as $permissionItem)
                                                    <div class="card-text">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                   name="display_name_permission[]"
                                                                   value="{{ $permissionItem->id }}">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                {{ $permissionItem->display_name }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Submit</button>


                        </form>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
