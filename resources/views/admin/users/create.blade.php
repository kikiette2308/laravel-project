@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Create User</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">User</li>
                </ol>

                <div class="container">
                    <div class="row justify-content-center">
                        <form action="{{ route('users.store') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Name</label>
                                    <input name="name" type="text" class="form-control" placeholder="Name..."
                                           value="{{old('name')}}">
                                    @error('name')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Email</label>
                                    <input autocomplete="off" name="email" type="email" class="form-control"
                                           placeholder="Email..." value="{{old('email')}}">
                                    @error('email')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Birthday</label>
                                    <input name="birthday" type="date" class="form-control"
                                           placeholder="Birthday..." value="{{old('birthday')}}">
                                    @error('birthday')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Address</label>
                                    <input name="address" type="text" class="form-control"
                                           placeholder="Address..." value="{{old('address')}}">
                                    @error('address')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Phone Number</label>
                                    <input name="phone_number" type="text" class="form-control"
                                           placeholder="Phone Number +84 ..." value="{{old('phone_number')}}">
                                    @error('phone_number')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Gender</label>
                                    <div>
                                        <input type="radio" name="gender" value="0" checked> Male<br>
                                        <input type="radio" name="gender" value="1"> Female<br>
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <div class="form-group">
                                    <label class="form-label">Password</label>
                                    <input autocomplete="off" name="password" type="password" class="form-control"
                                           placeholder="Password...">
                                    @error('password')
                                    <div class="link-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Role</label>
                                <div class="form">
                                    @foreach($roles as $role)
                                        <div class="row">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox"
                                                       name="display_name_permission[]" value="{{ $role->id }}">
                                                <label class="form-check-label" for="flexCheckDefault">
                                                    {{ $role->display_name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <button type="submit" class="btn btn-success">Create User</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
