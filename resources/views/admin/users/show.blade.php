@extends('admin.layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">List Users</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">All Users</li>
                </ol>
                <div class="container-fluid px-4">
                    {{--                    <a href="{{ route('users.create') }}" class="btn btn-success">Create</a>--}}
                    <div class="row justify-content-center">
                        <div class="col-md-4">
                            <form method="GET" class="m-md-3 d-flex">
                                <input type="text" name="key" class="form-control" placeholder="search ...">
                                <button type="submit" class="btn btn-primary">
                                    search
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Birthday</th>
                                <th scope="col">Address</th>
                                <th scope="col">Phone Number</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Roles</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">{{$user->id}}</th>
                                <td>{{$user->name}}</td>
                                <td>{{$user->birthday}}</td>
                                <td>{{$user->address}}</td>
                                <td>{{$user->phone_number}}</td>
                                <td>
                                    @if($user->gender == 0)
                                        {{'male'}}
                                    @else
                                        {{'female'}}
                                    @endif
                                </td>
                                <td>
                                    @foreach($user->roles as $role)
                                        <span class="badge badge-info">{{ $role->display_name }}</span>
                                    @endforeach
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </main>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; Your Website 2021</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
