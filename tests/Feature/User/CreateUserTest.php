<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
//    /** @test */
//    public function super_admin_can_see_create_user_form()
//    {
//        $this->loginWithSuperAdmin();
//        $response = $this->get(route('users.create'));
//        $response->assertViewIs('admin.users.create');
//        $response->assertSee('name')->assertSee('email');
//        $response->assertStatus(Response::HTTP_OK);
//    }

    /** @test */
    public function super_admin_can_create_new_user()
    {
        $this->loginWithSuperAdmin();
        $name = Str::random(10);
        $email = Str::random(10) . '@gmail.com';
        $data = [
            'name' => $name,
            'email' => $email,
            'birthday' => '2000/04/09',
            'address' => 'my dinh',
            'phone_number' => '+84963634044',
            'gender' => 0,
            'password' => Hash::make('12345678'),
        ];
        $response = $this->post(route('users.store'), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseHas('users', ['email' => $email, 'name' => $name]);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_and_email_and_password_are_null()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory([
            'name' => null,
            'email' => null,
            'password' => null
        ])->make()->toArray();
        $response = $this->post(route('users.store'), $user);
        $response->assertSessionHasErrors('name')
            ->assertSessionHasErrors('email')
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_create_user_form()
    {
        $this->loginWithUser();
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_user_form()
    {
        $this->loginUserWithPermission('user_create');
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.create');
        $response->assertSee('name')->assertSee('email');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_user()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.store'), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_new_user_form()
    {
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
