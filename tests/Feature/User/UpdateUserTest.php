<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_edit_form()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.edit');
        $response->assertSee($user->name)->assertSee($user->email);
    }

    /** @test */
    public function super_admin_can_update_user()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $data = [
            'name' => 'Huyen ABC',
            'email' => 'Huyenabc@gmail.com',
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
//        $response->assertRedirect(route('users.index'));
//        $this->assertDatabaseHas('users', ['name' => 'Huyen ABC']);
    }

    /** @test */
    public function super_admin_can_not_update_user_name_and_email_are_null()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null,
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertSessionHasErrors(['name', 'email']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_edit_user_form()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.edit');
        $response->assertSee('name')->assertSee('email');
    }

    /** @test */
    public function authenticated_user_have_permission_can_update_user()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $data = [
            'name' => 'Huyen ABC',
            'email' => 'Huyenabc@gmail.com',
            'password' => Hash::make('12345678'),
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
//        $response->assertRedirect(route('users.index'));
//        $this->assertDatabaseHas('users', ['name' => 'HuyenABC']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_email_if_name_are_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $data = [
            'name' => null,
            'email' => null
        ];
        $response = $this->put(route('users.update', $user->id), $data);
        $response->assertSessionHasErrors(['name', 'email']);
        $this->assertDatabaseMissing('users', $data);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.edit', $user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
