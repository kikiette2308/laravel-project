<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetASingleUserTest extends TestCase
{
    /** @test */
    public function super_admin_get_single_user()
    {
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.show');
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_single_user()
    {
        $this->loginUserWithPermission('user_show');
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.show');
        $response->assertSee($user->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_single_user()
    {
        $user = User::factory()->create();
        $response = $this->get(route('users.show', $user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }


}
