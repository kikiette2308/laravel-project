<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateNewCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_create_new_category()
    {
        $this->loginUserWithPermission('category_create');
        $createData = [
            'name' => 'Huyen ABC',
            'parent_id' => 1,
        ];
        $response = $this->postJson('/categories', $createData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Create Category Successfully!')
                ->etc()
            );
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_category()
    {
        $this->loginWithSuperAdmin();
        $createData = [
            'name' => 'Huyen ABC',
            'parent_id' => 1,
        ];
        $response = $this->postJson('/categories', $createData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) => $json->where('status', Response::HTTP_OK)
                ->where('message', 'Create Category Successfully!')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_category()
    {
        $createData = [
            'name' => 'Huyen ABC',
            'parent_id' => 1,
        ];
        $response = $this->postJson('/categories', $createData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginUserWithPermission('category_create');
        $createData = [
            'parent_id' => 1,
        ];
        $response = $this->postJson('/categories', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginWithSuperAdmin();
        $createData = [
            'parent_id' => 1,
        ];
        $response = $this->postJson('/categories', $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
