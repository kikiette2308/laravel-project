<?php

namespace Tests\Feature\Product;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateNewProductTest extends TestCase
{
    use WithFaker;

    public function __getDataValidate($data = [])
    {
        return array_merge([
            'name' => $this->faker->name,
            'price' => '10000',
            'description' => $this->faker->text,
            'category_id' => Category::factory()->create()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_create_product_view()
    {
        $this->loginWithSuperAdmin();

        $response = $this->get($this->getCreateProductRoute());

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('admin.products.create');

        $response->assertSee('name')->assertSee('price');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_product_view()
    {
        $response = $this->get($this->getCreateProductRoute());

        $response->assertRedirect('/login');

        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_product()
    {
        $this->loginWithSuperAdmin();

        $data = $this->__getDataValidate();

        $productCountBefore = Product::count();

        $response = $this->post($this->getStoreProductRoute(), $data);

        $productCountAfter = Product::count();

        $this->assertEquals($productCountBefore + 1, $productCountAfter);

        $this->assertDatabaseHas('products', ['name' => $data['name']]);

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function unauthenticated_user_can_not_create_product()
    {
        $data = $this->__getDataValidate();

        $response = $this->post($this->getStoreProductRoute(), $data);

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect('/login');
    }

    /** @test */
    public function product_be_long_category()
    {
        $product = Product::factory()->create();

        $category = Category::factory()->create();

        $product->categories()->sync($category->pluck('id'));

        $this->assertDatabaseHas('category_product', ['category_id' => $category->id, 'product_id' => $product->id]);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_product_if_name_and_price_is_null()
    {
        $this->loginWithSuperAdmin();

        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();

        $response = $this->post(route('products.store'), $product);

        $response->assertSessionHasErrors('name')->assertSessionHasErrors('price');
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_product_form()
    {
        $this->loginUserWithPermission('product_create');
        $response = $this->get(route('products.create'));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.products.create');
        $response->assertSee('name')->assertSee('price');
    }

    /** @test */
    public function authenticated_user_have_permission_can_create_new_product()
    {
        $this->loginUserWithPermission('product_store');

        $data = $this->__getDataValidate();

        $response = $this->post(route('products.store'), $data);

        $this->assertDatabaseHas('products', ['name' => $data['name']]);

        $response->assertStatus(Response::HTTP_FOUND);

        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_product_if_name_and_price_are_null()
    {
        $this->loginUserWithPermission('product_store');

        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();

        $response = $this->post(route('products.store'), $product);

        $response->assertSessionHasErrors('name')->assertSessionHasErrors('price');
    }

    public function getCreateProductRoute()
    {
        return route('products.create');
    }

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function getListProductRoute()
    {
        return route('products.index');
    }
}
