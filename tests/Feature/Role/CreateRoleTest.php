<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_see_create_role_form()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee('name')->assertSee('display_name');
    }

    /** @test */
    public function super_admin_can_create_new_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_name_and_are_null()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory([
            'name' => null,
            'display_name' => 1,
        ])->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_role_form()
    {
        $this->loginUserWithPermission('role_create');
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.create');
        $response->assertSee('name');
    }

    /** @test */
    public function authenticated_user_have_permission_can_create_new_role()
    {
        $this->loginUserWithPermission('role_create');
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('roles', $role);
        $response->assertRedirect(route('roles.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_role_if_name_and_display_name_are_null()
    {
        $this->loginUserWithPermission('role_create');
        $role = Role::factory([
            'name' => null,
            'display_name' => 1,
        ])->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_form()
    {
        $response = $this->get(route('roles.create'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_role()
    {
        $role = Role::factory()->make()->toArray();
        $response = $this->post(route('roles.store'), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
        $this->assertDatabaseMissing('roles', $role);
    }
}
