<?php

namespace Tests\Feature;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetASingRoleTest extends TestCase
{
    /** @test */
    public function super_admin_can_get_single_role()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.show', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_single_role()
    {
        $this->loginUserWithPermission('role_view');
        $role = Role::factory()->create();
        $response = $this->get(route('roles.show', $role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_single_role()
    {
        $role = Role::factory()->create();
        $response = $this->get(route('roles.show', $role->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }
}
