<?php

namespace App\Services;

use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected UserRepository $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function store($request)
    {
        $dataCreate = $request->all();

        $dataCreate['password'] = Hash::make($request->password);

        $user = $this->userRepository->create($dataCreate);

        $user->attachRole($request->display_name_permission);

        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->find($id);

        if ($request->password === null) {
            $dataUpdate = $request->except('password');
        } else {
            $dataUpdate = $request->all();
            $dataUpdate['password'] = Hash::make($request->password);
        }

        $user->update($dataUpdate);
        $user->syncRole($request->display_name_permission);

        return $user;
    }

    public function getAllUser($request)
    {
        $dataSearch = $request->all();

        $dataSearch['name'] = $request->name ?? '';

        $dataSearch['email'] = $request->name ?? '';

        $dataSearch['role_id'] = $request->role_id ?? '';

        return $this->userRepository->search($dataSearch)->appends($request->all());
    }

    public function getAll()
    {
        return $this->userRepository->getAll();
    }

    public function getById($id)
    {
        return $this->userRepository->find($id);
    }

    public function delete($id)
    {
        $user = $this->userRepository->find($id);
        $user->detachRole();
        $user->delete();
        return $user;
    }

    public function getRoleIdUser($id)
    {
        return $this->userRepository->getRoleId($id);
    }

    public function count()
    {
        return $this->userRepository->count();
    }
}
