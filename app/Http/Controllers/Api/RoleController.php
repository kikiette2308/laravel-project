<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\RoleRequest;
use App\Http\Resources\RoleCollection;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    protected $role;

    /**
     * @param $role
     */
    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->paginate(5);

        $rolesCollection = new RoleCollection($roles);

        return $this->successResponse($rolesCollection, 'Danh sách sản phẩm!');
    }

    public function store(RoleRequest $request)
    {
        $createRole = $request->all();
        $role = $this->role->create($createRole);
        $RoleResource = new RoleResource($role);
        return $this->successResponse($RoleResource, 'Thêm thành công!');
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
