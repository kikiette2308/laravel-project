<?php

namespace App\Http\Requests\User;

use App\Rules\VNPhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'birthday' => 'required',
            'email' => 'required|email|unique:users,email,'. $this->id,
            'password' => 'sometimes',
            'address' => 'required',
            'phone_number' => ['required', new VNPhoneRule()]
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute not be empty!',
            'unique' => 'email does exit!'
        ];
    }
}
