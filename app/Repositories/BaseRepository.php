<?php

namespace App\Repositories;

use App\Models\User;

use App\Models\Role;

abstract class BaseRepository
{
    protected $model;

    /**
     * @param $model
     */
    public function __construct()
    {
        $this->model = app($this->model());
    }

    abstract public function model();

    public function getAll()
    {
        return $this->model->all();
    }

    public function count()
    {
        return $this->model->all()->count();
    }

    public function create($dataCreate)
    {
        return $this->model->create($dataCreate);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function delete($dataDelete)
    {
        return $this->model->delete($dataDelete);
    }
}
