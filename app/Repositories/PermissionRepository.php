<?php

namespace App\Repositories;

use App\Models\Permisson;

class PermissionRepository extends BaseRepository
{
    public function model()
    {
        return Permisson::class;
    }

    //getWithGroup
    public function getWithGroup()
    {
        return $this->model->all()->groupBy('group_name');
    }
}
