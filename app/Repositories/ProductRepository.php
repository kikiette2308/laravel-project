<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        $name = $dataSearch['name'];

        $category = $dataSearch['category_id'];

        return $this->model->withName($name)->withCategoryId($category)->latest('id')->paginate(5);
    }
}
